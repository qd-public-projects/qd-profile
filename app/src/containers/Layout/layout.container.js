import React from "react";
import { useStaticQuery, graphql } from "gatsby";

import Layout from "../../components/Elements/layout";

const LayoutContainer = ({ location, children }) => {
  const Data = useStaticQuery(graphql`
    query {
      header: allFile(filter: { name: { eq: "header" } }) {
        nodes {
          childHomeJson {
            brandlogo {
              src
              alt
            }
            brandlogosticky {
              src
              alt
            }
            Items
            navbarlist {
              id
              title
              href
            }
          }
        }
      }
      footer: allFile(filter: { name: { eq: "footer" } }) {
        nodes {
          childHomeJson {
            delay
            Items
            footerlogo {
              src
              alt
            }
            footercol2 {
              title
              description {
                content
              }
            }
            footercol3 {
              title
              footerlinks {
                id
                title
                href
              }
            }
            footercol4 {
              title
              footeraddress {
                id
                title
                image {
                  src
                  alt
                }
              }
            }
            lowerfooter {
              footername
              footerpara
              footerlink
            }
          }
        }
      }
    }
  `);
  return (
    <Layout
      header={Data.header.nodes}
      footer={Data.footer.nodes}
      location={location}
      children={children}
    />
  );
};

export default LayoutContainer;
