import { graphql, useStaticQuery } from "gatsby";
import React from "react";
import Carousel from "../../components/Modules/Carousel/index";

const CarouselData = ({ block }) => {
  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark(
        filter: { fileAbsolutePath: { regex: "/(carousel)/" } }
        sort: { fields: [frontmatter___date], order: DESC }
      ) {
        nodes {
          fields {
            slug
          }
          frontmatter {
            id
            author
            carouselImage {
              src
              alt
            }
            date
            title
          }
        }
      }
    }
  `);
  return <Carousel carouselData={data.allMarkdownRemark.nodes} block={block} />;
};

export default CarouselData;
