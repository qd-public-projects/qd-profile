import React from "react";
import "../components/Foundations/layout.css";
import NotFound from "../components/Modules/PageNotFound/NotFound";
import GlobalStyle from "../components/Modules/PageNotFound/NotFound/global-styles";
import Fonts from "../components/Modules/PageNotFound/NotFound/fonts";

const NotFoundPage = () => (
  <div>
    <Fonts />
    <GlobalStyle />
    <NotFound />
  </div>
);

export default NotFoundPage;
