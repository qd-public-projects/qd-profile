import React from "react";

import ListView from "../components/Modules/ListView/index";

//👇 This default export determines where your story goes in the story list
export default {
  /* 👇 The "title" prop is optional.
   * See https://storybook.js.org/docsreact/configure/overview#configure-story-loading
   * to learn how to generate automatic "title"s
   */
  title: "Sections/List View",
  component: ListView,
};

//👇 We create a “template” of how args map to rendering
const Template = (args) => <ListView {...args} />;

export const Default = Template.bind({});
Default.args = {
  //👇 The args you need here will depend on your component

  block: {
    title: "Lorem Ipsum",
    delay: 4,
    listViewData: [
      {
        title: "Lorem Ipsum is simply",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        backImage: {
          alt: "Lorem Ipsum",
          src: "/img/download.png",
        },
        frontImage: {
          src: "/img/dummy-image-square.jpg",
          alt: "Lorem Ipsum",
        },
      },
      {
        title: "Lorem Ipsum is simply",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        backImage: {
          alt: "Lorem Ipsum",
          src: "/img/dummy-image-square.jpg",
        },
        frontImage: {
          src: "/img/download.png",
          alt: "Lorem Ipsum",
        },
      },
      {
        title: "Lorem Ipsum is simply",
        description:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        backImage: {
          alt: "Lorem Ipsum",
          src: "/img/download.png",
        },
        frontImage: {
          src: "/img/dummy-image-square.jpg",
          alt: "Lorem Ipsum",
        },
      },
    ],
  },
};
