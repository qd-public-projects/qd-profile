import React from "react";
import "antd/dist/antd.css";
import TabsView from "../components/Modules/TabsView/index";

//👇 This default export determines where your story goes in the story list
export default {
  /* 👇 The "title" prop is optional.
   * See https://storybook.js.org/docsreact/configure/overview#configure-story-loading
   * to learn how to generate automatic "title"s
   */
  title: "Sections/TabsView",
  component: TabsView,
};
//👇 We create a “template” of how args map to rendering
const Template = (args) => <TabsView {...args} />;

export const Default = Template.bind({});
Default.args = {
  //👇 The args you need here will depend on your component

  block: {
    title: "Lorem Ipsum",
    delay: -2,
    tablist: [
      {
        id: 1,
        title: "Contrary",
        detailtitle: "Contrary to popular belief",
        description:
          "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
        image: {
          src: "./img/dummy-image-square.jpg",
          alt: "Contrary",
        },
        sublist: [
          {
            title: "Why do we use it?",
            description:
              "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
            image: {
              src: "./img/download.png",
              alt: "Contrary",
            },
          },
        ],
      },
      {
        id: 2,
        title: "Lorem Ipsum",
        detailtitle: "Lorem Ipsum to popular belief",
        description:
          "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC",
        image: {
          src: "./img/download.png",
          alt: "Contrary",
        },
        sublist: [
          {
            title: "Why do we use it?",
            description:
              "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
            image: {
              src: "./img/dummy-image-square.jpg",
              alt: "Contrary",
            },
          },
        ],
      },
    ],
  },
};
