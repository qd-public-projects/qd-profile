import React from "react";

import Headermenu from "../components/Modules/HeaderMenu/index";

//👇 This default export determines where your story goes in the story list
export default {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docsreact/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: "Foundation/Header",
  component: Headermenu,
};

//👇 We create a “template” of how args map to rendering
const Template = (args) => <Headermenu headerJson={args.headerJson} />;

export const Default = Template.bind({});
Default.args = {
  //👇 The args you need here will depend on your component

  headerJson: [
    {
      childHomeJson: {
        brandlogo: {
          src: "/img/dummylogo.png",
          alt: "brand logo",
        },
        brandlogosticky: {
          src: "/img/dummylogo.png",
          alt: "logo sticky",
        },
        Items: ["loremipsum"],
        navbarlist: [
          {
            id: 1,
            title: "Lorem Ipsum",
            href: "#loremipsum",
          },
        ],
      },
    },
  ],
};
