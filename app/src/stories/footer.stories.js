import React from "react";

import Footer from "../components/Modules/Footer/index";

//👇 This default export determines where your story goes in the story list
export default {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docsreact/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: "Foundation/Footer",
  component: Footer,
};

//👇 We create a “template” of how args map to rendering
const Template = (args) => <Footer footerJson={args.footerJson} />;

export const Default = Template.bind({});
Default.args = {
  //👇 The args you need here will depend on your component

  footerJson: [
    {
      childHomeJson: {
        delay: 50,
        Items: ["loremIpsum"],
        footerlogo: {
          src: "/img/dummylogo.png",
          alt: "Lorem Ipsum",
        },
        footercol2: {
          title: "Lorem Ipsum",
          description: [
            {
              content:
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
            },
          ],
        },
        footercol3: {
          title: "Lorem Ipsum Links",
          footerlinks: [
            {
              id: 1,
              title: "Lorem Ipsum",
              href: "#loremIpsum",
            },
          ],
        },
        footercol4: {
          title: "Lorem Ipsum Address",
          footeraddress: [
            {
              id: 1,
              title: "+12-2222-22",
              image: {
                src: "/img/call.svg",
                alt: "Lorem Ipsum",
              },
            },
            {
              id: 2,
              title: "loremipsum@ioremipsum",
              image: {
                src: "/img/mail.svg",
                alt: "Lorem Ipsum",
              },
            },
            {
              id: 3,
              title: "Lorem Ipsum Address",
              image: {
                src: "/img/address.svg",
                alt: "Lorem Ipsum",
              },
            },
          ],
        },
        lowerfooter: {
          footername: "Lorem Ipsum",
          footerpara: "© Copyright: ",
          footerlink: "https://www.lipsum.com/",
        },
      },
    },
  ],
};
