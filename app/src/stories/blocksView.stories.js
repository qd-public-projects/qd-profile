import React from "react";

import BlocksView from "../components/Modules/BlocksView/index";

//👇 This default export determines where your story goes in the story list
export default {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docsreact/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: "Sections/Blocks View",
  component: BlocksView,
};

//👇 We create a “template” of how args map to rendering
const Template = (args) => <BlocksView {...args} />;

export const Default = Template.bind({});
Default.args = {
  //👇 The args you need here will depend on your component

  block: {
    title: "Lorem ipsum dolor sit amet consectetur",
    delay: 50,
    blocksViewList: [
      {
        title: "The standard Lorem Ipsum",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
        image: {
          src: "./img/dummy-image-square.jpg",
          alt: "customer",
        },
      },
      {
        title: "Section 1.10.33 of de Finibus",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
        image: {
          src: "./img/download.png",
          alt: "continuous",
        },
      },
      {
        title: "1914 translation by H. Rackham",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
        image: {
          src: "/img/dummy-image-square.jpg",
          alt: "improvement",
        },
      },
    ],
  },
};
