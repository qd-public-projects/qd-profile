import React from "react";

import Banner from "../components/Modules/Banner";

//👇 This default export determines where your story goes in the story list
export default {
  /* 👇 The title prop is optional.
   * See https://storybook.js.org/docsreact/configure/overview#configure-story-loading
   * to learn how to generate automatic titles
   */
  title: "Sections/Banner",
  component: Banner,
};

//👇 We create a “template” of how args map to rendering
const Template = (args) => <Banner {...args} />;

export const Default = Template.bind({});
Default.args = {
  //👇 The args you need here will depend on your component

  block: {
    title: "The standard Lorem Ipsum passage, used since the 1500s",
    subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    image: {
      src: "./img/dummy-image-square.jpg",
      alt: "banner",
    },
  },
};
