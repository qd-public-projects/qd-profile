import React from "react";

import Carousel from "../components/Modules/Carousel/index";

//👇 This default export determines where your story goes in the story list
export default {
  /* 👇 The "title" prop is optional.
   * See https://storybook.js.org/docsreact/configure/overview#configure-story-loading
   * to learn how to generate automatic "title"s
   */
  title: "Sections/Carousel",
  component: Carousel,
};

//👇 We create a “template” of how args map to rendering
const Template = (args) => <Carousel {...args} />;

export const Default = Template.bind({});

Default.args = {
  //👇 The args you need here will depend on your component
  block: {
    title: "Lorem Ipsum",
  },
  carouselData: [
    {
      fields: {
        slug: "lorem-ipsum-is-simply-dummy",
      },
      frontmatter: {
        id: 1,
        title: "What is Lorem Ipsum?",
        date: "December 13, 2021 4:41 PM",
        author: "Sit amet",
        carouselImage: {
          src: "/img/download.png",
          alt: " sitamet",
        },
      },
    },
    {
      fields: {
        slug: "lorem-ipsum-is-simply-dummy",
      },
      frontmatter: {
        id: 2,
        title: "What is Lorem Ipsum?",
        date: "December 13, 2021 4:41 PM",
        author: "Sit amet",
        carouselImage: {
          src: "/img/download.png",
          alt: " sitamet",
        },
      },
    },
    {
      fields: {
        slug: "lorem-ipsum-is-simply-dummy",
      },
      frontmatter: {
        id: 3,
        title: "What is Lorem Ipsum?",
        date: "December 13, 2021 4:41 PM",
        author: "Sit amet",
        carouselImage: {
          src: "/img/download.png",
          alt: " sitamet",
        },
      },
    },
    {
      fields: {
        slug: "lorem-ipsum-is-simply-dummy",
      },
      frontmatter: {
        id: 4,
        title: "What is Lorem Ipsum?",
        date: "December 13, 2021 4:41 PM",
        author: "Sit amet",
        carouselImage: {
          src: "/img/download.png",
          alt: " sitamet",
        },
      },
    },
  ],
};
