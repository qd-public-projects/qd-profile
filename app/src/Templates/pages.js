import React from "react";
import Layout from "../containers/Layout/layout.container";
import Seo from "../components/Elements/seo";
import Components from "../components/Modules/dynamicComponents";
import { graphql } from "gatsby";

const IndexPage = ({ data, location }) => {
  const componentSequence = data.markdownRemark.frontmatter;
  return (
    <Layout location={location}>
      <Seo
        title="Company Name | Lorem Ipsum"
        description="Lorem Ipsum"
      />

      {componentSequence.sectionitem.map((block) => (
        <div key={block.title}> {Components(block)}</div>
      ))}
    </Layout>
  );
};

export const pageQuery = graphql`
  query($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      frontmatter {
        sectionitem {
          type
          title
          subtitle
          description
          image {
            src
            alt
          }
          delay
          listViewData {
            title
            description
            backImage {
              src
              alt
            }
            frontImage {
              src
              alt
            }
          }
          tablist {
            id
            title
            detailtitle
            description
            image {
              src
              alt
            }
            sublist {
              title
              description
              image {
                src
                alt
              }
            }
          }
          blocksViewList {
            title
            description
            image {
              src
              alt
            }
          }
        }
      }
    }
  }
`;

export default IndexPage;
