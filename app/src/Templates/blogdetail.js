import React from "react";
import Fade from "react-reveal/Fade";
import {
  SectionContainer,
  SectionRow,
  SectionCol,
} from "../components/Foundations/grid";
import { graphql } from "gatsby";

import Layout from "../containers/Layout/layout.container";

import {
  BlogsDetailSection,
  BlogDetailHeading,
  BlogDetailSubHeading,
  BlogDetailParagraph,
  BlogDetailImage,
  BlogImageContainer,
  BlogDetailContainer,
  BlogDate,
  AuthorContainer,
  BlogDetailHeadingContainer,
  AuthorInfo,
} from "../components/Modules/BlogsDetail/blogs_detail.style";
const Blogsdetail = ({ data, location }) => {
  const data1 = data.markdownRemark.frontmatter;
  const post = data.markdownRemark;
  return (
    <Layout location={location}>
      <BlogDetailHeadingContainer>
        <SectionContainer>
          <SectionRow>
            <SectionCol sm={24}>
              <BlogDetailHeading>Blog Detail</BlogDetailHeading>
            </SectionCol>
          </SectionRow>
        </SectionContainer>
      </BlogDetailHeadingContainer>
      <BlogsDetailSection>
        <SectionContainer>
          <SectionRow>
            <SectionCol lg={16} md={12} sm={24}>
              <BlogDetailContainer>
                <Fade bottom delay={50}>
                  <BlogDetailSubHeading>{data1.title}</BlogDetailSubHeading>
                  <BlogDate>{data1.date}</BlogDate>
                  <BlogImageContainer>
                    <Fade bottom delay={50}>
                      <BlogDetailImage
                        src={data1.carouselImage.src}
                        alt={data1.carouselImage.alt}
                      />
                    </Fade>
                  </BlogImageContainer>
                  <BlogDetailParagraph>{data1.description}</BlogDetailParagraph>
                </Fade>
                <div
                  dangerouslySetInnerHTML={{ __html: post.html }}
                  itemProp="articleBody"
                />
                <AuthorContainer>
                  <AuthorInfo>Written By: {data1.author}</AuthorInfo>
                </AuthorContainer>
              </BlogDetailContainer>
            </SectionCol>
          </SectionRow>
        </SectionContainer>
      </BlogsDetailSection>
    </Layout>
  );
};

export default Blogsdetail;

export const pageQuery = graphql`
  query BlogPostBySlug($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date
        author
        carouselImage {
          src
          alt
        }
        description
      }
    }
  }
`;
