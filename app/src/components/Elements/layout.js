import * as React from "react";
import { SiteLayout } from "../Foundations/grid";

import HeaderMenu from "../Modules/HeaderMenu";
import Footer from "../Modules/Footer";
import Fonts from "../Foundations/fonts";
import GlobalStyle from "../Foundations/global-styles";

import "../Foundations/layout.css";

const Layout = ({ location, children, header, footer }) => {
  const rootPath = `${__PATH_PREFIX__}/`;
  const isRootPath = location.pathname === rootPath;
  const { Content } = SiteLayout;

  return (
    <SiteLayout className="global-wrapper" data-is-root-path={isRootPath}>
      <Fonts />
      <GlobalStyle />
      <HeaderMenu rootLocation={isRootPath} headerJson={header} />
      <Content>{children}</Content>
      <Footer rootLocation={isRootPath} footerJson={footer} />
    </SiteLayout>
  );
};

export default Layout;
