import * as React from "react";
import { render, screen } from "@testing-library/react";

import { Tab, TabPanel } from "../../Foundations/tab";

describe(`Tab`, () => {
  it(`Render List Correctly`, () => {
    const list = [
      { id: 1, title: "Hello 1" },
      { id: 2, title: "Hello 2" },
      { id: 3, title: "Hello 3" },
    ];

    render(
      <Tab>
        {list.map((item) => (
          <TabPanel tab={item.title} key={item.id}>
            content of {item.title}
          </TabPanel>
        ))}
      </Tab>
    );
    expect(screen.getAllByRole("tablist")).toMatchSnapshot();
  });
});
