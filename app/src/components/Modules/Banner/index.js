import React from "react";
import {
  BannerSection,
  BannerContents,
  BannerContentLeft,
  BannerContentRight,
  BannerImg,
  BannerPara,
  BannerH3,
  BannerH4,
} from "./banner.style";
import { SectionContainer } from "../../Foundations/grid";
import Fade from "react-reveal/Fade";

const Banner = ({ block }) => {
  const bannerData = block;

  return (
    <BannerSection id="homeContainer">
      <SectionContainer>
        <BannerContents key={bannerData.title}>
          <BannerContentLeft>
            <Fade delay={bannerData.delay}>
              <BannerH3>{bannerData.title}</BannerH3>
              <BannerH4>{bannerData.subtitle}</BannerH4>
              <BannerPara>{bannerData.description}</BannerPara>
            </Fade>
          </BannerContentLeft>
          <BannerContentRight>
            <Fade delay={bannerData.delay}>
              <BannerImg
                src={bannerData.image.src}
                alt={bannerData.image.alt}
              />
            </Fade>
          </BannerContentRight>
        </BannerContents>
      </SectionContainer>
    </BannerSection>
  );
};

export default Banner;
