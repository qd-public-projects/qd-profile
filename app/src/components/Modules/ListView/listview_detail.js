import React from "react";
import Fade from "react-reveal/Fade";
import { SectionCol } from "../../Foundations/grid";
import {
  ListViewRightHeading,
  ListViewInner,
  ListViewLeft,
  ListViewRight,
  ListViewLeftHeading,
  ListViewLeftPara,
  ListViewRightImgFirst,
  ListViewLeftImgFirst,
  ListViewLeftImgSecond,
  ListViewRightImgSecond,
  ListViewRowReverse,
  ListViewRow,
} from "./ListView.style";

const ListView_detail = ({ items, index, delay }) => {
  return (
    <ListViewInner key={index}>
      {index % 2 === 1 ? (
        <ListViewRow>
          <SectionCol md={14}>
            <Fade left delay={1 * delay}>
              <ListViewRight style={{ textAlign: "left" }}>
                <ListViewLeftImgFirst
                  src={items.backImage.src}
                  alt={items.backImage.alt}
                />
                <ListViewLeftImgSecond
                  src={items.frontImage.src}
                  alt={items.frontImage.alt}
                />
              </ListViewRight>
            </Fade>
          </SectionCol>
          <SectionCol md={10}>
            <ListViewLeft>
              <Fade right delay={1 * delay}>
                <ListViewRightHeading>{items.title}</ListViewRightHeading>
              </Fade>
              <Fade right delay={2 * delay}>
                <ListViewLeftPara>{items.description}</ListViewLeftPara>
              </Fade>
            </ListViewLeft>
          </SectionCol>
        </ListViewRow>
      ) : (
        <ListViewRowReverse>
          <SectionCol md={14}>
            <Fade right delay={1 * delay}>
              <ListViewRight>
                <ListViewRightImgFirst
                  src={items.backImage.src}
                  alt={items.backImage.alt}
                />
                <ListViewRightImgSecond
                  src={items.frontImage.src}
                  alt={items.frontImage.alt}
                />
              </ListViewRight>
            </Fade>
          </SectionCol>

          <SectionCol md={10}>
            <ListViewLeft>
              <Fade left delay={1 * delay}>
                <ListViewLeftHeading>{items.title}</ListViewLeftHeading>
              </Fade>
              <Fade left delay={1 * delay}>
                <ListViewLeftPara>{items.description}</ListViewLeftPara>
              </Fade>
            </ListViewLeft>
          </SectionCol>
        </ListViewRowReverse>
      )}
    </ListViewInner>
  );
};

export default ListView_detail;
