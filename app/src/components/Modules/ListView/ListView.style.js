import styled from "styled-components";
import { device } from "../../Foundations/device";
import {
  Commonpara,
  Commonbtn,
  SectionHeading,
} from "../../Foundations/common.style";
import { SectionRow } from "../../Foundations/grid";

export const ListViewSection = styled.section`
  padding: 100px 0px;
  background: #fff;

  @media ${device.tablet} {
    padding: 80px 10px;
  }
`;

export const ListViewHeadingWrapper = styled.div`
  text-align: center;
  max-width: 600px;
  margin: auto;
`;

export const ListViewHeading = styled(SectionHeading)`
  line-height: 30px;
  margin-bottom: 20px;

  @media ${device.tablet} {
    margin-bottom: 0px;
  }
`;

export const ListViewInner = styled.div`
  padding-top: 60px;
`;

export const ListViewRowReverse = styled(SectionRow)`
  flex-direction: row-reverse;
  padding-bottom: 80px;
  align-items: center;

  @media ${device.tablet} {
    padding-bottom: 80px;
    :last-child {
      padding-bottom: 0px;
    }
  }
`;

export const ListViewRowReverse2 = styled(SectionRow)`
  flex-direction: row-reverse;
  padding-bottom: 0px;
  align-items: center;

  @media ${device.tablet} {
    padding-bottom: 80px;
    :last-child {
      padding-bottom: 0px;
    }
  }
`;

export const ListViewRight = styled.div`
  position: relative;
  text-align: right;

  @media ${device.tablet} {
    text-align: left;
  }
`;

export const ListViewRightImgFirst = styled.img`
  width: calc(100% - 100px);
  height: auto;
  border-radius: 10px;
  box-shadow: 0px 0px 26px 5px #d0d0d0;
  position: relative;
  right: 30px;
  margin-bottom: 30px;

  @media ${device.laptop} {
    width: calc(100% - 50px);
  }

  @media ${device.tablet} {
    width: calc(100% - 30px);
    right: 0px;
  }
`;

export const ListViewRightImgSecond = styled.img`
  position: absolute;
  bottom: 0px;
  right: 0px;
  border-radius: 10px;
  box-shadow: 0px 0px 26px 5px #d0d0d0;
  margin-bottom: 0px;
  max-width: 50%;
`;

export const ListViewLeft = styled.div`
  @media ${device.tablet} {
    text-align: left;
  }
`;

export const ListViewLeftHeading = styled(SectionHeading)`
  text-align: left;

  @media ${device.laptop} {
    font-size: 26px;
  }
  @media ${device.tablet} {
    font-size: 30px;
    margin-top: 80px;
  }
`;

export const ListViewRightHeading = styled(SectionHeading)`
  text-align: left;

  @media ${device.laptop} {
    font-size: 26px;
  }
  @media ${device.tablet} {
    font-size: 30px;
    margin-top: 40px;
  }
`;

export const ListViewLeftPara = styled(Commonpara)`
  text-align: left;
  font-size: 14px;
`;

export const ListViewBtn = styled(Commonbtn)``;

export const ListViewRow = styled(SectionRow)`
  padding-bottom: 80px;
  align-items: center;
`;

export const ListViewLeftImgFirst = styled.img`
  width: calc(100% - 100px);
  border-radius: 10px;
  box-shadow: 0px 0px 26px 5px #d0d0d0;
  position: relative;
  margin-bottom: 0px;

  @media ${device.laptop} {
    width: calc(100% - 50px);
  }
  @media ${device.tablet} {
    width: calc(100% - 30px);
  }
  @media ${device.mobileL} {
  }
`;

export const ListViewLeftImgSecond = styled.img`
  position: absolute;
  bottom: 0px;
  right: 70px;
  border-radius: 10px;
  box-shadow: 0px 0px 26px 5px #d0d0d0;
  margin-bottom: 0px;
  max-width: 60%;

  @media ${device.laptop} {
    right: 20px;
  }
  @media ${device.tablet} {
    right: 0px;
  }
`;
