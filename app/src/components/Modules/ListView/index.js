import React from "react";
import {
  ListViewSection,
  ListViewHeadingWrapper,
  ListViewHeading,
} from "./ListView.style";
import {
  SectionContainer,
  SectionRow,
  SectionCol,
} from "../../Foundations/grid";
import ListViewDetail from "./listview_detail";
import Fade from "react-reveal/Fade";

const ListView = ({ block }) => {
  const listViewContent = block;

  return (
    <ListViewSection id="ListViewContainer">
      <SectionContainer key={listViewContent.title}>
        <SectionRow>
          <SectionCol md={24}>
            <ListViewHeadingWrapper>
              <Fade bottom delay={1 * listViewContent.delay}>
                <ListViewHeading>{listViewContent.title}</ListViewHeading>
              </Fade>
            </ListViewHeadingWrapper>
          </SectionCol>
        </SectionRow>
        {listViewContent.listViewData.map((items, index) => (
          <ListViewDetail
            items={items}
            index={index}
            key={index}
            delay={listViewContent.delay}
          />
        ))}
      </SectionContainer>
    </ListViewSection>
  );
};
export default ListView;
