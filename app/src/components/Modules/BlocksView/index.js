import React from "react";
import { BlocksViewCard } from "../../Foundations/card";
import {
  SectionContainer,
  SectionRow,
  SectionCol,
} from "../../Foundations/grid";
import {
  BlocksViewSection,
  BlocksViewSectionHeading,
  BlocksViewIcon,
  BlocksViewHeading,
  BlocksViewCustomContainer,
  BlocksViewDesc,
} from "./blocksview.style";
import Fade from "react-reveal/Fade";

const BlocksView = ({ block }) => {
  const blocksViewData = block;
  return (
    <BlocksViewSection id="featuresContainer">
      <SectionContainer key={blocksViewData.title}>
        <SectionRow>
          <SectionCol sm={24}>
            <Fade bottom delay={blocksViewData.delay}>
              <BlocksViewSectionHeading>
                {blocksViewData.title}
              </BlocksViewSectionHeading>
            </Fade>
          </SectionCol>
        </SectionRow>
        <BlocksViewCustomContainer>
          <SectionRow>
            {blocksViewData.blocksViewList.map((item, idx) => {
              return (
                <SectionCol sm={24} md={12} lg={8} key={item.title}>
                  <Fade bottom delay={(idx + 1) * blocksViewData.delay}>
                    <BlocksViewCard
                      cover={
                        <BlocksViewIcon
                          alt={item.image.alt}
                          src={item.image.src}
                        />
                      }
                    >
                      <BlocksViewHeading>{item.title}</BlocksViewHeading>
                      <BlocksViewDesc>{item.description}</BlocksViewDesc>
                    </BlocksViewCard>
                  </Fade>
                </SectionCol>
              );
            })}
          </SectionRow>
        </BlocksViewCustomContainer>
      </SectionContainer>
    </BlocksViewSection>
  );
};

export default BlocksView;
