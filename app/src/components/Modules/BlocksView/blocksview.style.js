import styled from "styled-components";
import {
  SectionHeading,
  Commonh4,
  Commonpara,
} from "../../Foundations/common.style";
import { device } from "../../Foundations/device";

export const BlocksViewSection = styled.section`
  padding: 100px 0px;
  background: #f9fafc;
  position: relative;

  @media ${device.tablet} {
    padding: 80px 10px;
  }
`;

export const BlocksViewCustomContainer = styled.div``;

export const BlocksViewSectionHeading = styled(SectionHeading)`
  margin-bottom: 20px;

  @media ${device.tablet} {
    margin-bottom: 0px;
  }
`;

export const BlocksViewIcon = styled.img`
  height: 75px;
  margin-bottom: 10px;
`;

export const BlocksViewHeading = styled(Commonh4)`
  color: #3a3a3a;
  font-size: 22px;
  text-transform: uppercase;
  font-weight: 500;
`;
export const BlocksViewDesc = styled(Commonpara)`
  color: #7a7f83;
  margin-bottom: 5px;
  font-size: 15px;
`;
