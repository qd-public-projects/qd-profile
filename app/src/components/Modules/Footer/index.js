import React from "react";
import {
  SectionContainer,
  SectionRow,
  SectionCol,
} from "../../Foundations/grid";
import {
  FooterSection,
  FooterCol,
  FooterTitle,
  FooterPara,
  FooterLi,
  FooterSocial,
  BottomFooterPara,
  BottomFooterRight,
  FooterAddWrapper,
  FooterAddLeft,
  FooterAddRight,
  FooterAddImg,
  FooterAddText,
  BottomFooterWrapper,
  BottomLink,
  FooterLogo,
  TwitterIcon,
  LinkedinIcon,
  FooterAnchorMenu,
} from "./footer.style";
import Fade from "react-reveal/Fade";

import ScrollSpy from "react-scrollspy";

const Footer = ({ rootLocation, footerJson }) => {
  const islocation = rootLocation;
  return (
    <div>
      <FooterSection>
        {footerJson.map((items) => (
          <SectionContainer key={items.childHomeJson.footerlogo.alt}>
            <Fade bottom delay={1 * items.childHomeJson.delay}>
              <SectionRow>
                <SectionCol xs={24} sm={24} md={6}>
                  <FooterCol>
                    <FooterLogo
                      src={items.childHomeJson.footerlogo.src}
                      alt={items.childHomeJson.footerlogo.alt}
                    />
                  </FooterCol>
                </SectionCol>
                <SectionCol xs={24} sm={24} md={6}>
                  <FooterCol>
                    <FooterTitle>
                      {items.childHomeJson.footercol2.title}
                    </FooterTitle>
                    {items.childHomeJson.footercol2.description.map((item) => {
                      return (
                        <FooterPara key={items.childHomeJson.footercol2.title}>
                          {item.content}
                        </FooterPara>
                      );
                    })}
                  </FooterCol>
                </SectionCol>
                <SectionCol xs={24} sm={24} md={6}>
                  <FooterCol>
                    <FooterTitle>
                      {items.childHomeJson.footercol3.title}
                    </FooterTitle>
                    <ScrollSpy
                      offset={-59}
                      items={items.childHomeJson.Items}
                      currentClassName="is-current"
                    >
                      {items.childHomeJson.footercol3.footerlinks.map(
                        (item) => {
                          return (
                            <FooterLi key={item.id}>
                              <FooterAnchorMenu
                                offset={55}
                                href={islocation ? item.href : `/${item.href}`}
                              >
                                {item.title}
                              </FooterAnchorMenu>
                            </FooterLi>
                          );
                        }
                      )}
                    </ScrollSpy>
                  </FooterCol>
                </SectionCol>
                <SectionCol xs={24} sm={24} md={6}>
                  <FooterCol>
                    <FooterTitle>
                      {items.childHomeJson.footercol4.title}
                    </FooterTitle>
                    {items.childHomeJson.footercol4.footeraddress.map(
                      (item) => {
                        return (
                          <FooterAddWrapper key={item.id}>
                            <FooterAddLeft>
                              <FooterAddImg
                                src={item.image.src}
                                alt={item.image.alt}
                              />
                            </FooterAddLeft>
                            <FooterAddRight>
                              <FooterAddText>{item.title}</FooterAddText>
                            </FooterAddRight>
                          </FooterAddWrapper>
                        );
                      }
                    )}
                  </FooterCol>
                </SectionCol>
              </SectionRow>
            </Fade>
          </SectionContainer>
        ))}
      </FooterSection>
      <BottomFooterWrapper>
        {footerJson.map((items) => (
          <SectionContainer key={items.childHomeJson.lowerfooter.footername}>
            <Fade bottom delay={2 * items.childHomeJson.delay}>
              <SectionRow>
                <SectionCol xs={24} sm={24}>
                  <BottomFooterPara>
                    {items.childHomeJson.lowerfooter.footerpara}
                    <BottomLink
                      rel="noreferrer"
                      href={items.childHomeJson.lowerfooter.footerlink}
                      target="_blank"
                    >
                      {items.childHomeJson.lowerfooter.footername}
                    </BottomLink>
                  </BottomFooterPara>
                </SectionCol>
                <SectionCol xs={24} sm={24}>
                  <BottomFooterRight>
                    <FooterSocial
                      href="https://www.lipsum.com/"
                      aria-label="Twitter Link"
                    >
                      <TwitterIcon />
                    </FooterSocial>
                    <FooterSocial
                      href="https://www.lipsum.com/"
                      aria-label="Linkedin Link"
                    >
                      <LinkedinIcon />
                    </FooterSocial>
                  </BottomFooterRight>
                </SectionCol>
              </SectionRow>
            </Fade>
          </SectionContainer>
        ))}
      </BottomFooterWrapper>
    </div>
  );
};

export default Footer;
