import styled from "styled-components";

import { SectionHeading } from "../../Foundations/common.style";
import { device } from "../../Foundations/device";

export const TabsViewSection = styled.section`
  background-color: #fff;
  padding: 100px 0;

  @media ${device.tablet} {
    padding: 80px 10px;
  }
`;

export const TabsViewHeading = styled(SectionHeading)`
  margin-bottom: 40px;

  @media ${device.tablet} {
    margin-bottom: 0px;
  }
`;

export const TabsViewContentItem = styled.div``;

export const TabsViewContent = styled.div``;

export const TabsViewContentHeading = styled.h2`
  font-weight: 700;
  font-size: 25px;
  transition: 1s ease-in-out;
`;

export const TabsViewContentPara = styled.p`
  color: rgb(122, 127, 131);
`;

export const TabsViewContentUl = styled.ul`
  list-style-type: none;
`;

export const TabsViewContentList = styled.li`
  margin-bottom: 40px;
`;

export const TabsViewContentListAnchor = styled.a`
  display: flex;
  text-decoration: none;
  align-items: center;
`;

export const TabsViewAnchorImgContainer = styled.div`
  margin-right: 20px;
  background: #dfe1e5;
  padding: 10px 15px;
`;

export const TabsViewAnchorImg = styled.img`
  width: 28px;
  transition: 1s ease-in-out;
`;

export const TabsViewAnchorTextContainer = styled.div`
  align-items: center;
`;

export const TabsViewAnchorTextHeading = styled.h4`
  margin-bottom: 0px;
  font-size: 14px;
  color: black;
  transition: 1s ease-in-out;
  font-weight: 700;
`;

export const TabsViewAnchorTextPara = styled.p`
  margin-bottom: 0px;
  font-size: 12px;
  transition: 1s ease-in-out;
  color: rgb(122, 127, 131);
`;

export const TabsViewContentImgContainer = styled.div`
  text-align: right;

  @media ${device.laptop} {
    text-align: center !important;
  }
`;
export const TabsViewContentImg = styled.img`
  width: 70%;
`;
