import React from "react";
import { SectionRow, SectionCol } from "../../Foundations/grid";
import Fade from "react-reveal/Zoom";

import {
  TabsViewContentItem,
  TabsViewContent,
  TabsViewContentHeading,
  TabsViewContentPara,
  TabsViewContentUl,
  TabsViewContentList,
  TabsViewContentListAnchor,
  TabsViewAnchorImgContainer,
  TabsViewAnchorImg,
  TabsViewAnchorTextContainer,
  TabsViewAnchorTextHeading,
  TabsViewAnchorTextPara,
  TabsViewContentImgContainer,
  TabsViewContentImg,
} from "./TabsView.style";

const TabsViewContents = ({ items, delay }) => {
  return (
    <TabsViewContentItem>
      <SectionRow>
        <SectionCol sm={24} md={24} lg={12}>
          <TabsViewContent>
            <Fade delay={delay}>
              <TabsViewContentHeading>
                {items.detailtitle}
              </TabsViewContentHeading>

              <TabsViewContentPara>{items.description}</TabsViewContentPara>
              <TabsViewContentUl>
                {items.sublist.map((item) => (
                  <TabsViewContentList key={item.title}>
                    <TabsViewContentListAnchor href="#dsd">
                      <TabsViewAnchorImgContainer>
                        <TabsViewAnchorImg
                          src={item.image.src}
                          alt={item.image.alt}
                        />
                      </TabsViewAnchorImgContainer>
                      <TabsViewAnchorTextContainer>
                        <TabsViewAnchorTextHeading>
                          {item.title}
                        </TabsViewAnchorTextHeading>
                        <TabsViewAnchorTextPara>
                          {item.description}
                        </TabsViewAnchorTextPara>
                      </TabsViewAnchorTextContainer>
                    </TabsViewContentListAnchor>
                  </TabsViewContentList>
                ))}
              </TabsViewContentUl>
            </Fade>
          </TabsViewContent>
        </SectionCol>
        <SectionCol sm={24} md={24} lg={12}>
          <TabsViewContentImgContainer
            className="ContentImage"
            style={{ textAlign: "right", marginTop: "30px" }}
          >
            <Fade delay={100}>
              <TabsViewContentImg src={items.image.src} alt={items.image.alt} />
            </Fade>
          </TabsViewContentImgContainer>
        </SectionCol>
      </SectionRow>
    </TabsViewContentItem>
  );
};

export default TabsViewContents;
