import React, { Component } from "react";
import {
  SectionContainer,
  SectionRow,
  SectionCol,
} from "../../Foundations/grid";
import Fade from "react-reveal/Fade";
import { Tab, TabPanel } from "../../Foundations/tab";
import { TabsViewSection, TabsViewHeading } from "./TabsView.style";
import TabsViewContents from "./tabsViewContent";

class TabsView extends Component {
  render() {
    const tabsViewData = this.props.block;
    return (
      <TabsViewSection id="TabsView">
        <SectionContainer key={tabsViewData.title}>
          <SectionRow>
            <SectionCol md={24}>
              <Fade bottom delay={tabsViewData.delay}>
                <TabsViewHeading>{tabsViewData.title}</TabsViewHeading>
              </Fade>
            </SectionCol>
          </SectionRow>
          <SectionRow>
            <Tab>
              {tabsViewData.tablist.map((item) => (
                <TabPanel tab={item.title} key={item.id}>
                  <TabsViewContents items={item} delay={tabsViewData.delay} />
                </TabPanel>
              ))}
            </Tab>
          </SectionRow>
        </SectionContainer>
      </TabsViewSection>
    );
  }
}

export default TabsView;
