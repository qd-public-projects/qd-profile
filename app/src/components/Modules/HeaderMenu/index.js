import React, { Component } from "react";
import {
  Headerwrapper,
  NavCustom,
  UlCustom,
  LiCustomLogo,
  BrandLogo,
  LiCustomItem,
  LiCustomToggle,
  AnchorCustom,
  MenuIcon,
  HeaderAnchorMenu,
} from "./headermenu.style";
import { SectionContainer } from "../../Foundations/grid";
import ScrollSpy from "react-scrollspy";

class Headermenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stickyClass: "top",
      isActive: false,
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", () => {
      let stickyClass = "topSticky";
      let scrollPos = window.scrollY;

      if (scrollPos < 100) {
        stickyClass = "top";
      }

      if (this.state.stickyClass !== stickyClass) {
        this.setState({ stickyClass });
      }
    });
  }

  componentWillUnmount() {
    window.removeEventListener("scroll");
  }

  menuClick = () => {
    this.setState({
      isActive: !this.state.isActive,
    });
  };

  render() {
    const data = this.props.headerJson;
    const islocation = this.props.rootLocation;
    return (
      <Headerwrapper
        isSticky={this.state.stickyClass === "topSticky" ? true : false}
      >
        <NavCustom
          isSticky={this.state.stickyClass === "topSticky" ? true : false}
        >
          {data.map((items) => (
            <SectionContainer key={items.childHomeJson.brandlogo.alt}>
              <UlCustom>
                <LiCustomLogo
                  isSticky={
                    this.state.stickyClass === "topSticky" ? true : false
                  }
                >
                  <BrandLogo
                    src={
                      this.state.stickyClass === "topSticky"
                        ? items.childHomeJson.brandlogosticky.src
                        : items.childHomeJson.brandlogo.src
                    }
                    isSticky={
                      this.state.stickyClass === "topSticky" ? true : false
                    }
                    alt={
                      this.state.stickyClass === "topSticky"
                        ? items.childHomeJson.brandlogosticky.alt
                        : items.childHomeJson.brandlogo.alt
                    }
                  />
                </LiCustomLogo>

                <LiCustomItem
                  className={this.state.isActive ? "active-main-li" : ""}
                  isSticky={
                    this.state.stickyClass === "topSticky" ? true : false
                  }
                >
                  <ScrollSpy
                    offset={-59}
                    items={items.childHomeJson.Items}
                    currentClassName="is-current"
                  >
                    {items.childHomeJson.navbarlist.map((item) => {
                      return (
                        <li
                          className={this.state.isActive ? "active" : ""}
                          key={item.id}
                        >
                          <HeaderAnchorMenu
                            offset={55}
                            href={islocation ? item.href : `/${item.href}`}
                          >
                            {item.title}
                          </HeaderAnchorMenu>
                        </li>
                      );
                    })}
                  </ScrollSpy>
                </LiCustomItem>

                <LiCustomToggle>
                  <AnchorCustom onClick={this.menuClick}>
                    <MenuIcon
                      isSticky={
                        this.state.stickyClass === "topSticky" ? true : false
                      }
                    />
                  </AnchorCustom>
                </LiCustomToggle>
              </UlCustom>
            </SectionContainer>
          ))}
        </NavCustom>
      </Headerwrapper>
    );
  }
}

export default Headermenu;
