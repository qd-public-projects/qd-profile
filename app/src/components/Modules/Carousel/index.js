import React, { Component } from "react";
import {
  SectionContainer,
  SectionRow,
  SectionCol,
} from "../../Foundations/grid";
import { CarouselCard } from "../../Foundations/card";
import Fade from "react-reveal/Fade";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import {
  CarouselSection,
  CarouselHeading,
  CarouselImage,
  CarouselImageAnchor,
  CarouselTextHeading,
  CarouselTextHeadingAnchor,
  CarouselMetaHome,
  CarouselMetaLeft,
  CarouselMetaLeftPara,
  CarouselMetaRight,
  CarouselMetaRightPara,
  CarouselButtonContainer,
  CarouselButtonNext,
  CarouselButtonPrev,
} from "./carousel.style";

class Carousel extends Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }
  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  render() {
    var settings = {
      dots: false,
      infinite: true,
      margin: 30,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    const date = (i) => {
      const str1 = i.frontmatter.date.substring(0, 3);
      const str2 = i.frontmatter.date.slice(-16);
      return str1 + " " + str2;
    };
    const data = this.props.carouselData;
    const carouselTitle = this.props.block;
    return (
      <CarouselSection id="carousel">
        <SectionContainer>
          <SectionRow>
            <SectionCol md={24}>
              <Fade bottom delay={50}>
                <CarouselHeading>{carouselTitle.title}</CarouselHeading>
              </Fade>
            </SectionCol>
          </SectionRow>
          <SectionRow>
            <SectionCol lg={24}>
              <Slider ref={(c) => (this.slider = c)} {...settings}>
                {data.map((i) => (
                  <Fade bottom delay={50} key={i.frontmatter.id}>
                    <CarouselCard
                      cover={
                        <CarouselImageAnchor href={i.fields.slug}>
                          <CarouselImage
                            src={i.frontmatter.carouselImage.src}
                            alt={i.frontmatter.carouselImage.alt}
                          />
                        </CarouselImageAnchor>
                      }
                    >
                      <CarouselTextHeading>
                        <CarouselTextHeadingAnchor href={i.fields.slug}>
                          {i.frontmatter.title}
                        </CarouselTextHeadingAnchor>
                      </CarouselTextHeading>
                      <CarouselMetaHome>
                        <CarouselMetaLeft>
                          <CarouselMetaLeftPara>{date(i)}</CarouselMetaLeftPara>
                        </CarouselMetaLeft>
                        <CarouselMetaRight>
                          <CarouselMetaRightPara>
                            By: {i.frontmatter.author}
                          </CarouselMetaRightPara>
                        </CarouselMetaRight>
                      </CarouselMetaHome>
                    </CarouselCard>
                  </Fade>
                ))}
              </Slider>
              <Fade bottom delay={50}>
                <CarouselButtonContainer style={{ textAlign: "center" }}>
                  <CarouselButtonPrev onClick={this.previous}>
                    Prev
                  </CarouselButtonPrev>
                  <CarouselButtonNext onClick={this.next}>
                    Next
                  </CarouselButtonNext>
                </CarouselButtonContainer>
              </Fade>
            </SectionCol>
          </SectionRow>
        </SectionContainer>
      </CarouselSection>
    );
  }
}

export default Carousel;
