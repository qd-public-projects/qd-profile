import styled from "styled-components";
import { SectionHeading } from "../../Foundations/common.style";
import { device } from "../../Foundations/device";
import { Time } from "styled-icons/boxicons-regular/Time";

export const CarouselSection = styled.section`
  background-color: #fff;
  padding: 100px 0;

  @media ${device.tablet} {
    padding: 80px 10px;
  }
`;

export const CarouselHeading = styled(SectionHeading)`
  margin-bottom: 40px;

  @media ${device.tablet} {
    margin-bottom: 40px;
  }
`;

export const CarouselImageAnchor = styled.a``;

export const CarouselImage = styled.img`
  width: 100%;
  margin: 0;
`;

export const CarouselTextHeading = styled.h3`
  font-size: 18px;
  color: black;
  letter-spacing: 1px;
  margin-bottom: 15px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  display: inline-block;
  text-transform: capitalize;
  font-weight: 600;
`;

export const CarouselTextHeadingAnchor = styled.a`
  color: black;
  font-size: 18px;
  text-decoration: none;
  outline: none;
`;

export const CarouselMetaHome = styled.div`
  padding-top: 15px;
  border-top: 3px solid #f0f0ff;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  -ms-flex-line-pack: center;
  align-content: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
`;

export const CarouselMetaLeft = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 1;
  -ms-flex: 1;
  flex: 1;
`;

export const CarouselMetaLeftPara = styled.p`
  font-size: 15px;
  margin-bottom: 0;
`;

export const CarouselMetaRight = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-flex: 1;
  -ms-flex: 1;
  flex: 1;
  -webkit-box-pack: end;
  -ms-flex-pack: end;
  justify-content: end;
`;

export const CarouselMetaRightPara = styled.p`
  margin-left: 15px;
  font-size: 15px;
  margin-bottom: 0;
`;

export const CarouselMetaRightIcon = styled(Time)`
  margin-right: 5px;
  color: #0571f7b8;
`;

export const CarouselButtonContainer = styled.div`
  text-align: center;
  margin-top: 30px;
`;

export const CarouselButtonNext = styled.button`
  background-color: #f9fafc;
  border: none;
  font-size: 16px;
  cursor: pointer;
  outline: none;
  border-radius: 10px;
  padding: 5px 10px;
`;

export const CarouselButtonPrev = styled.button`
  background-color: #0571f7b8;
  border: none;
  outline: none;
  font-size: 16px;
  cursor: pointer;
  margin-right: 10px;
  color: white;
  border-radius: 10px;
  padding: 5px 10px;
`;
