import React from "react";
import Banner from "./Banner";
import BlocksView from "./BlocksView";
import ListView from "./ListView";
import TabsView from "./TabsView";
import CarouselData from "../../containers/Carousel/carouselData";

const Components = {
  banner: Banner,
  blocksView: BlocksView,
  listView: ListView,
  tabsView: TabsView,
  carousel: CarouselData,
};

const dynamicComponent = (block) => {
  if (typeof Components[block.type] !== "undefined") {
    return React.createElement(Components[block.type], {
      block: block,
    });
  }
  return React.createElement(() => (
    <div>The component {block.type} has not been created Yet.</div>
  ));
};

export default dynamicComponent;
