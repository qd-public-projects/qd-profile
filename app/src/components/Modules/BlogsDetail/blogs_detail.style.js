import styled from "styled-components";
import { SectionHeading } from "../../Foundations/common.style";
import { device } from "../../Foundations/device";

export const BlogsDetailSection = styled.section`
  padding: 100px 0;
  background-color: #fff;

  @media ${device.tablet} {
    padding: 80px 10px;
  }
`;

export const BlogDetailHeading = styled(SectionHeading)`
  margin: 40px 0;
  text-align: left;
  @media ${device.tablet} {
    margin-bottom: 0px;
  }
`;
export const BlogDetailSubHeading = styled.h3`
  font-size: 24px;
  color: black;
  letter-spacing: 1px;
  margin-bottom: 15px;
  -webkit-transition: all 0.4s ease 0s;
  transition: all 0.4s ease 0s;
  display: inline-block;
  text-transform: capitalize;
  font-weight: 700;
`;

export const BlogDetailParagraph = styled.p`
  font-size: 16px;
  color: #7a7f83;
`;

export const BlogDetailImage = styled.img`
  width: 100%;
`;

export const BlogImageContainer = styled.div``;

export const BlogDetailContainer = styled.div``;

export const HomeButton = styled.a`
  text-decoration: none;
  background-color: #0571f7b8;
  margin: 40px 10px;
  color: white;
  padding: 10px;
  border-radius: 5px;
`;
export const BlogDate = styled.p`
  font-size: 18px;
`;
export const AuthorContainer = styled.div`
  border-top: 1px solid #00000029;
  padding: 20px 0;
`;
export const AuthorInfo = styled.h4`
  font-size: 18px;
  font-weight: 400;
`;

export const BlogDetailHeadingContainer = styled.section`
  background-color: #f9fafc;
  padding: 100px 0;
  @media ${device.tablet} {
    padding: 80px 10px;
  }
`;
