import styled from "styled-components";
import { Card } from "antd";

export const CustomCard = styled(Card)``;

export const BlocksViewCard = styled(CustomCard)`
  background: transparent !important;
  border: none !important;
  text-align: center;
  padding: 60px 20px 0px;
`;

export const CarouselCard = styled(CustomCard)`
  margin: 0 10px !important;
`;
