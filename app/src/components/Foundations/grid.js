import styled from "styled-components";
import { Layout, Row, Col } from "antd";

export const SiteLayout = styled(Layout)`
  background: transparent !important;
`;

export const SectionContainer = styled.div`
  /* position: relative; */
  margin-left: auto;
  margin-right: auto;
  padding-right: 10px;
  padding-left: 10px;
  overflow: hidden;

  @media (min-width: 476px) {
    padding-right: 10px;
    padding-left: 10px;
  }
  @media (min-width: 768px) {
    padding-right: 10px;
    padding-left: 10px;
  }
  @media (min-width: 992px) {
    padding-right: 10px;
    padding-left: 10px;
  }
  @media (min-width: 1200px) {
    padding-right: 10px;
    padding-left: 10px;
  }
  @media (min-width: 476px) {
    width: 550px;
  }
  @media (min-width: 768px) {
    width: 720px;
    max-width: 100%;
  }
  @media (min-width: 992px) {
    width: 960px;
    max-width: 100%;
  }
  @media (min-width: 1200px) {
    width: 1140px;
    max-width: 100%;
  }
  @media (min-width: 1400px) {
    width: 1140px;
    max-width: 100%;
  }
`;

export const SectionRow = Row;
export const SectionCol = Col;
