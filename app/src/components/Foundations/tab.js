import styled from "styled-components";
import { Tabs } from "antd";

const { TabPane } = Tabs;

export const Tab = styled(Tabs)``;

export const TabPanel = styled(TabPane)``;
