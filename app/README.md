# Website Project

## Development Instructions

1. Clone repository

```bash
git clone git@gitlab.com:qd-public-projects/qd-profile.git
cd qd-profile/app

```

2. Install dependencies

```bash
yarn install
```

3. Deploy for development

```bash
yarn develop
```
After successful deployment open `http://localhost:8000/` in your browser
