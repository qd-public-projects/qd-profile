module "bucket" {
  source = "./modules/terraform_google_cloud_storage"

  names      = [var.name]
  project_id = var.project
  location   = var.location
  prefix     = ""

  lifecycle_rules = [{
    action = {
      type = "Delete"
    }
    condition = {
      age        = 365
      with_state = "ANY"
    }
  }]
  website = {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}


module "gce-lb-http" {
  source = "./modules/terraform_load_balancer"

  name                   = var.name
  project                = var.project
  website_domain_name    = var.name
}
