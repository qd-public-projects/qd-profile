variable "project" {
  type        = string
  default     = ""
  description = "Project name in GCP"
}

variable "zone" {
  type        = string
  default     = ""
  description = "Zone to be used in GCP"
}

variable "region" {
  type        = string
  default     = ""
  description = "Region to be used in GCP"
}

variable "location" {
  type        = string
  default     = ""
  description = "Location for multiple region to be used in GCP"
}

variable "name" {
  description = "Name of the buckets to create."
  type        = string
}

variable "network_name" {
  default = "tf-lb-https-redirect-nat"
}


