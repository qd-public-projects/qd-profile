resource "google_compute_global_address" "default" {
  project      = var.project
  name         = var.default_global_address != "" ? var.default_global_address : "${var.name}-address"
  ip_version   = "IPV4"
  address_type = "EXTERNAL"
}

# ------------------------------------------------------------------------------
# IF PLAIN HTTP ENABLED, CREATE FORWARDING RULE AND PROXY
# ------------------------------------------------------------------------------

resource "google_compute_target_http_proxy" "http" {
  count   = var.enable_http ? 1 : 0
  project = var.project
  name    = var.http_proxy != "" ? var.http_proxy : "${var.name}-http-proxy"
  url_map = google_compute_url_map.urlmapHttp.id
}

resource "google_compute_global_forwarding_rule" "http" {
  provider   = google-beta
  count      = var.enable_http ? 1 : 0
  project    = var.project
  name       = var.http_forwarding_rule != "" ? var.http_forwarding_rule : "${var.name}-http-rule"
  target     = google_compute_target_http_proxy.http[0].self_link
  ip_address = google_compute_global_address.default.address
  port_range = "80"

  depends_on = [google_compute_global_address.default]

  labels = var.custom_labels
}

# ------------------------------------------------------------------------------
# IF SSL ENABLED, CREATE FORWARDING RULE AND PROXY
# ------------------------------------------------------------------------------

resource "google_compute_global_forwarding_rule" "https" {
  provider   = google-beta
  project    = var.project
  count      = var.enable_ssl ? 1 : 0
  name       = var.https_forwarding_rule != "" ? var.https_forwarding_rule : "${var.name}-https-rule"
  target     = google_compute_target_https_proxy.default[0].self_link
  ip_address = google_compute_global_address.default.address
  port_range = "443"
  depends_on = [google_compute_global_address.default]

  labels = var.custom_labels
}

resource "google_compute_target_https_proxy" "default" {
  project = var.project
  count   = var.enable_ssl ? 1 : 0
  name    = var.default_proxy != "" ? var.default_proxy : "${var.name}-https-proxy"
  url_map = google_compute_url_map.urlmapHttps.id

  ssl_certificates = [google_compute_managed_ssl_certificate.default.id]
}

# ------------------------------------------------------------------------------
# PREPARE COMMONLY USED LOCALS
# ------------------------------------------------------------------------------

locals {
  # We have to use dashes instead of dots in the bucket name, because
  # that bucket is not a website
  website_domain_name_dashed = replace(var.website_domain_name, ".", "-")
}

# ------------------------------------------------------------------------------
# CREATE THE URL MAP WITH THE BACKEND BUCKET AS DEFAULT SERVICE
# ------------------------------------------------------------------------------

resource "google_compute_url_map" "urlmapHttps" {
  provider = google-beta
  project  = var.project

  name        = var.url_map_https
  description = "URL map for ${local.website_domain_name_dashed}"

  default_service = google_compute_backend_bucket.static.self_link
}

resource "google_compute_url_map" "urlmapHttp" {
  provider = google-beta
  project  = var.project

  name        = var.url_map_http
  description = "URL map for ${local.website_domain_name_dashed}"

  default_url_redirect {
    https_redirect         = true
    redirect_response_code = "MOVED_PERMANENTLY_DEFAULT"
    strip_query            = false
  }

}

# ------------------------------------------------------------------------------
# CREATE THE BACKEND BUCKET
# ------------------------------------------------------------------------------

resource "google_compute_backend_bucket" "static" {
  provider = google-beta
  project  = var.project

  name        = var.backend_bucket_name != "" ? var.backend_bucket_name : "${local.website_domain_name_dashed}-bucket"
  bucket_name = var.website_domain_name
  enable_cdn  = var.enable_cdn
}

resource "google_compute_managed_ssl_certificate" "default" {
  name = var.ssl_cert != "" ? var.ssl_cert : "${var.name}"

  managed {
    domains = [var.website_domain_name]
  }
}