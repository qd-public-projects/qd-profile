
# ------------------------------------------------------------------------------
# LOAD BALANCER OUTPUTS
# ------------------------------------------------------------------------------

output "load_balancer_ip_address" {
  description = "IP address of the Cloud Load Balancer"
  value       = google_compute_global_address.default.address
}


output "website_bucket_name" {
  description = "Name of the website bucket"
  value       = var.website_domain_name
}