# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These variables are expected to be passed in by the operator
# ---------------------------------------------------------------------------------------------------------------------

variable "project" {
  description = "The project ID to create the resources in."
  type        = string
}

variable "name" {
  description = "Name for the load balancer forwarding rule and prefix for supporting resources."
  type        = string
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL MODULE PARAMETERS
# These variables have defaults, but may be overridden by the operator.
# ---------------------------------------------------------------------------------------------------------------------


variable "enable_http" {
  description = "Set to true to enable plain http. Note that disabling http does not force SSL and/or redirect HTTP traffic. See https://issuetracker.google.com/issues/35904733"
  type        = bool
  default     = true
}

variable "custom_labels" {
  description = "A map of custom labels to apply to the resources. The key is the label name and the value is the label value."
  type        = map(string)
  default     = {}
}

variable "website_domain_name" {
  description = "The name of the website and the Cloud Storage bucket to create (e.g. static.foo.com)."
  type        = string
}

variable "enable_cdn" {
  description = "Set to `true` to enable cdn on website backend bucket."
  type        = bool
  default     = true
}

variable "url_map_http" {
  description = "The url map name of the website for Http"
  type        = string
}

variable "url_map_https" {
  description = "The url map name of the website for Https"
  type        = string
}

variable "backend_bucket_name" {
  description = "Backend Bucket name of the website"
  type        = string
  default     = ""
}

variable "default_global_address" {
  description = "Default global address of the website"
  type        = string
  default     = ""
}

variable "http_forwarding_rule" {
  description = "Forwarding rule for HTTP of the website"
  type        = string
  default     = ""
}

variable "https_forwarding_rule" {
  description = "Forwarding rule for HTTPs of the website"
  type        = string
  default     = ""
}

variable "http_proxy" {
  description = "Http Proxy of the website"
  type        = string
  default     = ""
}

variable "default_proxy" {
  description = "Default Proxy of the website"
  type        = string
  default     = ""
}

variable "enable_ssl" {
  description = "Set to true to enable ssl. If set to 'true', you will also have to provide 'var.ssl_certificates'."
  type        = bool
  default     = false
}

variable "ssl_cert" {
  description = "Default Certificate of the website"
  type        = string
  default     = ""
}