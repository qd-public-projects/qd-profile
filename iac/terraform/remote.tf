terraform {
  backend "gcs" {
    bucket = "your_backend_bucket"
    prefix = "prefix/state"
  }
}