---
name: raw
sectionitem:
  - type: banner
    title: The standard Lorem Ipsum passage, used since the 1500s
    subtitle: Lorem ipsum dolor sit amet, consectetur adipiscing elit
    delay: 50
    description: Lorem Ipsum is simply dummy text of the printing and typesetting
      industry. Lorem Ipsum has been the industry's standard dummy text ever
      since the 1500s, when an unknown printer took a galley of type and
      scrambled it to make a type specimen book.
    image:
      src: /img/dummy-image-square.jpg
      alt: Lorem Ipsum
  - type: listView
    title: Lorem Ipsum
    delay: 50
    listViewData:
      - title: Where does it come from?
        description: Contrary to popular belief, Lorem Ipsum is not simply random text.
          It has roots in a piece of classical Latin literature from 45 BC,
          making it over 2000 years old. Richard McClintock, a Latin professor
          at Hampden-Sydney College in Virginia, looked up one of the more
          obscure Latin words, consectetur, from a Lorem Ipsum passage.
        backImage:
          src: /img/download.png
          alt: lorem
        frontImage:
          src: /img/dummy-image-square.jpg
          alt: ipsum
      - title: Where can I get some?
        description: "There are many variations of passages of Lorem Ipsum available,
          but the majority have suffered alteration in some form, by injected
          humour, or randomised words which don't look even slightly believable.
          "
        backImage:
          src: /img/download.png
          alt: lorem
        frontImage:
          src: /img/dummy-image-square.jpg
          alt: lorem
  - type: tabsView
    title: Lorem Ipsum
    delay: 50
    tablist:
      - id: 1
        title: Contrary
        detailtitle: Contrary to popular belief
        description: Contrary to popular belief, Lorem Ipsum is not simply random text.
          It has roots in a piece of classical Latin literature from 45 BC
        image:
          src: /img/dummy-image-square.jpg
          alt: lorem
        sublist:
          - title: Why do we use it?
            description: It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout.
            image:
              src: /img/download.png
              alt: ipsum
      - id: 2
        title: Lorem Ipsum
        detailtitle: Lorem Ipsum to popular belief
        description: Contrary to popular belief, Lorem Ipsum is not simply random text.
          It has roots in a piece of classical Latin literature from 45 BC
        image:
          src: /img/download.png
          alt: lorem
        sublist:
          - title: Why do we use it?
            description: It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout.
            image:
              src: /img/dummy-image-square.jpg
              alt: ipsum
  - type: carousel
    title: Lorem Ipsum
  - type: blocksView
    title: Lorem ipsum dolor sit amet consectetur
    delay: 50
    blocksViewList:
      - title: The standard Lorem Ipsum
        description: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam.
        image:
          src: /img/dummy-image-square.jpg
          alt: lorem
      - title: Section 1.10.33 of de Finibus
        description: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam.
        image:
          src: /img/download.png
          alt: ipsum
      - title: 1914 translation by H. Rackham
        description: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam.
        image:
          src: /img/dummy-image-square.jpg
          alt: lorem
---
