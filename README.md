# QD - Profile
An open-source project that utilizes below mentioned technologies to build a ready to use Company profile and blog website. It was build as project-based learning for BS Computer science students and fresh graduates.

## Learning Goals
- React
- JAMStack
- Headless CMS
- Static website hosting

## Current Stack
### Dev Stack
- React
- Ant Design
- Styled Components
- Gatsby
- Netlify CMS
- Storybooks

### Pipeline
- Gitlab CI/CD

### IaC
- Terraform

### Deployment
- GCP
  - Load Balancer
  - Storage Bucket
  - CDN
  - SSL Certificate
   
## More to Come (InshaAllah)
- Test
  - Jest
  - Cypress
  - Puppeteer
  - Lighthouse

- Local Devbox
  - docker
  - docker-compose

- Deployment Options
  - AWS
  - Azure
  - DO

And hopefully much more :)
